# Marconi 16

A 16 key macro pad with 2 knobs. It takes inspiration from the
Elgato StreamDeck© and vintage radio design.

- [Build log with photos](https://imgur.com/gallery/msuap6K).
- [QMK firmware](https://gitlab.com/to1ne/qmk_firmware/-/tree/marconi16)

Designed by [Toon Claes](https://twitter.com/to1ne).

## Rev1

Fully working, but has a few flaws:

- The USB connector should be more on the outside, the bottom PCB makes it hard
  to plug in.
- The PCBs should be wider, to give the wood more material to clamp on.

[Reddit post](https://old.reddit.com/r/MechanicalKeyboards/comments/nt34mf/marconi16rev1/)
